import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.rdd.RDD
import util.{GlobalArgs, ScopeCaculateUtil}

/**
  * Created by chenjianwen on 2016/3/2.
  */
class YCFActionDataHandler(val path:String) {
    def ratingRdd():RDD[(Int,Rating)]={
      CFContext.sc.textFile(GlobalArgs.act_path).map(x=>{

        var res:List[((Int,Int),(Int,Long))] = List()
          x.split(",") match {
              case Array(actType,devId,userId,products,timestamp)=>{
                var tempUserId:Int =  0
                if("0".equals(userId)){
                  tempUserId = devId.hashCode;
                }else{
                  tempUserId = userId.toInt
                }
                //有可能一个行为数据对应多个订单
                val productss = products.split("#")
                for(one <- productss){
                  res = res :+((tempUserId,one.toInt),(actType.toInt,timestamp.toLong))
                  //println(res.size)
                }
              }
          }
        res
      }).flatMap(
        x=>x
      ).groupByKey()
        .map(pro=>{
         ((pro._2.iterator.next()._2%10).toInt,new Rating(pro._1._1,pro._1._2,ScopeCaculateUtil.actions2Socrecompute(pro._2)))
      })
    }


}

