package extension

import java.util.function.Consumer

import org.apache.spark.mllib.recommendation.Rating

/**
  * Created by chenjianwen on 2016/3/10.
  *
  * 使用隐式转换做语法扩展
  */
trait Extension{
  implicit def encloseToRunnable(a:()=>Unit):Runnable = {
    return new Runnable {
      override def run(): Unit = a()
    }
  }

  implicit class Incr(var e:Int){
    def ++():Int={
      e=e+1
      e
    }
  }






}


