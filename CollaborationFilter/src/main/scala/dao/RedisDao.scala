package dao

import redis.clients.jedis.{JedisPoolConfig, JedisPool, Jedis}
import util.GlobalArgs

/**
  * Created by chenjianwen on 2016/3/15.
  * 使用连接池获取  Redis的客户端
  */

object RedisDao{
  //def apply():RedisDao=new RedisDao


  @volatile private[this] var jedisPool:JedisPool = null

  def getJedis:Jedis={
   getJedisPool.getResource
  }

  private def getJedisPool:JedisPool={
    if(jedisPool != null) jedisPool
    val poolconf = new JedisPoolConfig
    //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。
    poolconf.setMaxIdle(10)
    //表示当borrow(引入|获取)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException
    poolconf.setMaxWaitMillis(500*1000)
    //连接池中的最大连接数量
    poolconf.setMaxTotal(15)
    //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    poolconf.setTestOnBorrow(true)
    jedisPool = new JedisPool(poolconf,GlobalArgs.REDIS_IP,GlobalArgs.REDIS_PORT.toInt)
    jedisPool
  }
}
